        <div class="package-card row" ng-class="{'package-card--awaiting-approval': !package.isApproved}">
            <a ng-href="\{{hrefPath}}">
                <div ng-show="!package.isApproved" class="package-card__awaiting-approval-text">
                    Awaiting Approval
                </div>
                <div class="package-card__image col-xs-12"
                     adorn="isAdorned"
                     ng-style="{'background-image': 'url(' + featureImage + ')'}">

                    <img
                        src="{{ path '/dist/icons/' }}new-package.svg"
                        ng-if="isNewPackage && !hideBadges && !isAdorned"
                        class="card-adornment card-adornment__new-package"
                        title="New package"
                    />

                    <img
                        src="{{ path '/dist/icons/' }}top-pick-vendor-search.svg"
                        ng-if="!isNewPackage && !package.vendor.exclusive && package.vendor.recommended && showVendor && !hideBadges && !isAdorned"
                        class="card-adornment card-adornment__top-pick"
                        title="Only on City Pantry"
                    />

                    <img
                        src="{{ path '/dist/icons/' }}exclusive-flag-right.svg"
                        ng-if="package.vendor.exclusive && showVendor && !hideBadges && !isAdorned"
                        class="card-adornment card-adornment__exclusive"
                        title="City Pantry Recommends"
                    />

                    <div ng-if="showVendor" class="package-card__vendor" itemprop="brand" itemscope
                         itemtype="http://schema.org/Brand" ng-click="goToVendor($event)">
                        <meta itemprop="logo" content="\{{ package.vendor.images[0].medium }}">
                        <span class="img-circle-xs" style="background-image: url('\{{ package.vendor.images[0].medium }}');"></span>
                        <span itemprop="name">&nbsp;by {{package.vendor.name}}</span>
                    </div>
                </div>
                <div class="package-card__details col-xs-12">
                    <div class="row">
                        <div class="package-card__name col-xs-12">
                            <div class="package-card__price pull-right">
                                \{{ package.pricePerPerson | currency:'£' }}
                            </div>
                            \{{ package.name | limitTo:35 }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul>
                                <li>\{{getPackagingTypeText()}}</li>
                                <li ng-repeat="item in getItemNames() track by $index">
                                    \{{item}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="package-card__diet-images">
                            <img ng-repeat="diet in diets" ng-src="\{{diet.icon}}"
                                 uib-tooltip="\{{diet.name}}" tooltip-append-to-body="true">
                        </div>
                    </div>
                </div>
            </a>
        </div>