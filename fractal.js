'use strict';

/*
* Require the path module
*/

const paths = {
  build: `${__dirname}/www`,
  src: `${__dirname}/src`,
  static: `${__dirname}/tmp`,
};

/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

/*
 * Give your project a title.
 */
fractal.set('project.title', 'Citypantry Frontend Lib');

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('default.preview', '@preview');
fractal.components.set('default.status', null);
fractal.components.set('ext', '.hbs');
fractal.components.set('path', `${paths.src}/components`);

/*
 * Tell Fractal where to look for documentation pages.
 */
//fractal.docs.set('path', `${paths.src}/docs`);

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path', paths.static);
fractal.web.set('builder.dest', paths.build);
fractal.web.set('builder.urls.ext', null);
