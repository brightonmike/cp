const pkg = require('./package.json');
const gulp = require('gulp');

// Fractal
const fractal = require('./fractal.js');
const logger = fractal.cli.console;

// Sass
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const nano = require('gulp-cssnano');

// utilities
const imagemin = require('gulp-imagemin');
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');

// Paths
const paths = {
  build: `${__dirname}/www`,
  dest: `${__dirname}/tmp`,
  src: `${__dirname}/src`,
  modules: `${__dirname}/node_modules`,
};

// Tasks

function build() {
  const builder = fractal.web.builder();

  builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
  builder.on('error', err => logger.error(err.message));

  return builder.build().then(() => {
    logger.success('Fractal build completed!');
  });
}

function watch(){
	serve();
	gulp.watch(`${paths.src}/assets/icons`, icons);
	gulp.watch(`${paths.src}/assets/images`, images);
	gulp.watch(`${paths.src}/**/*.css`, styles);
  gulp.watch(`${paths.src}/**/*.scss`, styles);	
}


// Serve dynamic site
function serve() {
  const server = fractal.web.server({
    sync: true,
  });

  server.on('error', err => logger.error(err.message));

  return server.start().then(() => {
    logger.success(`Fractal server is now running at ${server.url}`);
  });
}

// Clean
function clean() {
  return del(`${paths.dest}/dist/`);
}

// Icons
function icons() {
  return gulp.src(`${paths.src}/assets/icons/**/*`)
    // .pipe(imagemin())
    .pipe(gulp.dest(`${paths.dest}/dist/icons`));
}

// Images
function images() {
  return gulp.src(`${paths.src}/assets/images/**/*`)
    .pipe(imagemin({
      progressive: true,
    }))
    .pipe(gulp.dest(`${paths.dest}/dist/images`));
}

// Styles
function styles() {
  return gulp.src(`${paths.src}/assets/styles/*.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.dest + '/dist/styles'));
}

const compile = gulp.series(clean, gulp.parallel(icons, images, styles));

gulp.task('watch', gulp.series(compile, watch));
gulp.task('build', gulp.series(compile, build));